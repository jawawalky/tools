#!/bin/sh

# #############################################################################
#
# >>> PostgreSQL <<<
#
# Starts the 'postgres' application in a docker container.
#
# The following settings apply
#
#  - Admin User:             admin
#  - Admin Password:         secr3t
#  - Database Name:          demo
#  - Port:                   5432
#
# #############################################################################

echo ">>> Starting Postgres database as Docker container ..."

docker pull postgres/alpine
docker run \
    --name 'postgres-demo' \
    -p 5432:5432 \
    -e 'POSTGRES_USER=admin' \
    -e 'POSTGRES_PASSWORD=secr3t' \
    -e 'POSTGRES_DB=demo' \
    -d postgres

