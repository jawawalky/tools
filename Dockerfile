# Take latest version of Postgres database.

FROM postgres/alpine:latest

ENV POSTGRES_USER=admin
ENV POSTGRES_PASSWORD=secr3t
ENV POSTGRES_DB=demo

EXPOSE 5432

ENTRYPOINT ["postgres", "--name", "postgres-demo"]

